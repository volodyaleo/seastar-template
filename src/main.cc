
#include <seastar/core/app-template.hh>
#include <seastar/core/distributed.hh>
#include <seastar/core/print.hh>
#include <seastar/core/when_all.hh>
#include <vector>

#include "libexample.hpp"


int main(int ac, char** av) {
	seastar::app_template app;

	// Allocate memory here because lambda inside app.run will finish before
	// future expired (tasks finished) 

	std::vector<Example> examples;
	std::vector<seastar::future<>> futures;

	for (int i = 0; i < 4; i++)
		examples.emplace_back(i);

	return app.run(ac, av, [&] {
		for (int i = 0; i < 4; i++)
			futures.emplace_back(examples[i].do_example());
		
		// Returns from lambda immidiately
		// But reactor will wait until all futures expired
		return seastar::when_all_succeed(std::begin(futures), std::end(futures));
	});
}
