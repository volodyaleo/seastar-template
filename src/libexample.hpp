#pragma once

#include <seastar/core/future.hh>

class Example
{
public:
    explicit Example(int num) : m_number{num} {}

    seastar::future<> do_example();
private:
    int m_number = -1;
};
