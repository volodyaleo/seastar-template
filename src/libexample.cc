#include "libexample.hpp"

#include <iostream>
#include <seastar/core/sleep.hh>

// seastar/core/coroutine.hh header for co_await and co_return support
#include <seastar/core/coroutine.hh>

using namespace std::chrono_literals;

seastar::future<> Example::do_example()
{
    std::cout << "Do example " << m_number << " started" << std::endl;
    co_await seastar::sleep(1s);
    std::cout << "Do example " << m_number << " finished" << std::endl;
    co_return;
}
