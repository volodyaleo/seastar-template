#include <gtest/gtest.h>

#include <seastar/core/app-template.hh>
#include <seastar/core/seastar.hh>
#include <seastar/core/when_all.hh>
#include <seastar/core/thread.hh>
#include <seastar/core/reactor.hh>

#include "libexample.hpp"


class TestExample : public ::testing::Test {
protected:
	void SetUp() override {

	}

	void TearDown() override {

	}	
};


TEST_F(TestExample, SmokeTest) {
	std::vector<Example> examples;

	for (int i = 0; i < 4; i++)
		examples.emplace_back(i);

	std::vector<seastar::future<>> futures;

	for (int i = 0; i < 4; i++)
		futures.emplace_back(examples[i].do_example());

	// Add .get() to wait until coroutine finish
	seastar::when_all_succeed(std::begin(futures), std::end(futures)).get();

	ASSERT_EQ(1, 1);
}

// Test runner for seastar+gtest
int main(int argc, char **argv) {
	seastar::app_template app;

	return app.run(argc, argv, [&] {
		// Seastar does not allow future::get calls and locks to prevent engine deadlock
		auto f = seastar::async([] {
			::testing::InitGoogleTest();

			seastar::engine().exit(RUN_ALL_TESTS());
		});

		return f;
	});
}
